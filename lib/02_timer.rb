class Timer
  def initialize
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string
    return_time = ""
    return_time += "#{double_digit((@seconds/3600).to_s)}:"
    return_time += "#{double_digit((@seconds%3600/60).to_s)}:"
    return_time += "#{double_digit((@seconds%60).to_s)}"
    puts return_time
    return_time
  end

  def double_digit(str)
    str = "0" + str if str.length < 2
    str
  end
end
