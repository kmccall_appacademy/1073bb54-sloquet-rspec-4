class Temperature
  # TODO: your code goes here!

  def initialize(option)
    if option[:f]
      self.fahrenheit = option[:f]
    else
      self.celsius = option[:c]
    end
  end

  def fahrenheit=(temp)
    @temperature = self.class.ftoc(temp)
  end

  def celsius=(temp)
    @temperature = temp
  end

  def in_fahrenheit
    self.class.ctof(@temperature)
  end

  def in_celsius
    @temperature
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.ftoc(temp)
    @temperature = (temp - 32) * 5/9.0
  end

  def self.ctof(temp)
    @temperature = (temp * 9/5.0) + 32
  end
end

class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
