class Book
  # TODO: your code goes here!
  CONJ_AND_ARTICLES = %w(a and the an in of)

  def title=(title)
    @title = title
    words = @title.split(' ')
    words[0].capitalize!
    words.map! do |word|
      if CONJ_AND_ARTICLES.include?(word)
        word = word
      else
        word.capitalize
      end
    end
    @title = words.join(' ')
  end

  def title
    @title
  end
end
