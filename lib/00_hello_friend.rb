class Friend
  # TODO: your code goes here!
  def greeting(name = nil)
    return "Hello!" unless name
    "Hello, #{name}!"
  end
end
