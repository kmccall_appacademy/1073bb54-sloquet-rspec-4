class Dictionary
  # TODO: your code goes here!
  def initialize
    self.entries = {}
  end

  def entries=(hash)
    @entries = hash
  end

  def entries
    @entries
  end

  def keywords
    @entries.keys.sort
  end

  def add(hash_or_key)
    if hash_or_key.is_a?(Hash)
      @entries[hash_or_key.keys[0]] = hash_or_key.values[0]
    else
      @entries[hash_or_key] = nil
    end
  end

  def include?(word)
    @entries.keys.include?(word)
  end

  def find(word)
    matching_entries = {}
    @entries.keys.each {|key| matching_entries[key] = @entries[key] if key.include?(word)}
    matching_entries
  end

  def printable
    interface = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@entries[keyword]}"}
    end
    interface.join("\n")
  end

end
